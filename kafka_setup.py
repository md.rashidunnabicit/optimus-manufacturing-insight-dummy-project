from kafka.admin import KafkaAdminClient, NewTopic
from kafka import KafkaProducer
import json
from typing import List, Dict, Any

class KafkaSetup:
    def __init__(self, server: str):
        self.server = server
        self.admin_client = KafkaAdminClient(bootstrap_servers=server)
        self.producer = KafkaProducer(bootstrap_servers=server,
                                      value_serializer=lambda v: json.dumps(v).encode('utf-8'))

    def create_topics(self, topics: List[str], num_partitions: int, replication_factor: int):
        """
        Create multiple Kafka topics if they do not already exist.
        
        :param topics: List of topic names to create
        :param num_partitions: Number of partitions per topic
        :param replication_factor: Replication factor for topic durability
        """
        existing_topics = self.admin_client.list_topics()
        new_topics = [NewTopic(name, num_partitions=num_partitions, replication_factor=replication_factor)
                      for name in topics if name not in existing_topics]
        if new_topics:
            self.admin_client.create_topics(new_topics=new_topics)

    def send_data(self, topic: str, data: Dict[str, Any]):
        """
        Send data to a specific Kafka topic.

        :param topic: Topic name to which the data will be sent
        :param data: Dictionary containing the data to be sent
        """
        self.producer.send(topic, value=data)
        self.producer.flush()

    def close(self):
        """
        Close the producer and admin client connections cleanly.
        """
        self.producer.close()
        self.admin_client.close()

# Usage
if __name__ == "__main__":
    # Configuration and setup
    kafka_server = 'localhost:9092'
    topics = ['iot_sensor_data', 'iot_status_updates']
    partitions = 3
    replication = 2

    kafka_manager = KafkaSetup(server=kafka_server)
    kafka_manager.create_topics(topics=topics, num_partitions=partitions, replication_factor=replication)

    # Simulate sending data
    data = {
        'sensor_id': 123,
        'temperature': 22.5,
        'status': 'active',
        'timestamp': '2024-06-01T12:00:00'
    }
    kafka_manager.send_data(topic='iot_sensor_data', data=data)

    # Close connections
    kafka_manager.close()
