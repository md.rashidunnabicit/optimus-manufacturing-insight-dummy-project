# IoT Data Analysis and Machine Learning Repository

## Introduction

This repository is dedicated to the processing, analysis, and prediction of IoT data utilizing state-of-the-art technologies and machine learning techniques. It is designed for scalability and real-time analysis, incorporating a range of scripts and notebooks that effectively handle large streams of data from IoT devices.

## Problem Statement

The IoT industry generates vast amounts of data that are often underutilized due to challenges in effective data processing, analysis, and real-time prediction. This project addresses these challenges by providing robust solutions for managing IoT data streams, performing complex queries, and deploying predictive models in production environments.

## Technology Used

- **MongoDB**: NoSQL database used for storing IoT data.
- **Apache Kafka**: Stream-processing software platform used for handling real-time data feeds.
- **Python**: Primary programming language.
- **Pandas & NumPy**: For data manipulation and analysis.
- **Scikit-Learn**: Machine learning library for Python.
- **Flask**: Micro web framework for building web applications.
- **Seaborn & Matplotlib**: Visualization libraries.
- **Jupyter Notebook**: Interactive computing environment.

## Modules

1. **Kafka Setup**: Configures Kafka topics and producers for IoT data simulation.
2. **MongoDB Analysis**: Performs data aggregation and visualization on MongoDB datasets.
3. **Machine Learning Service**: Manages data preprocessing, model training, serialization, and provides a Flask API for predictions.

## Notebooks

- **IoT Data Processing.ipynb**: Demonstrates the end-to-end process of data collection, preprocessing, model training, evaluation, and deployment using a Jupyter Notebook.

## Installation and Setup
To set up and run the projects, follow these steps:

1. **Clone the Repository**:

git clone repository-url
    
## Installation and Setup
To set up and run the projects, follow these steps:

### Install Required Libraries

Ensure Python 3.6+ is installed, then run:

pip install pymongo kafka-python matplotlib seaborn scikit-learn flask pandas


### Setup MongoDB and Kafka
Ensure instances of MongoDB and Kafka are running. Configure the connections within each script according to your setup.

### Environment Variables
Set up necessary environment variables or config files as required by the scripts.

## Project Structure

- `kafka_setup.py`: Sets up Kafka topics and simulates IoT data production.
- `query_optimization_mongodb.py`: Script for optimizing and visualizing data queries from MongoDB.
- `iot_ml_service.py`: Contains the Flask app and machine learning service for IoT data prediction.
- `IoT_Data_Processing.ipynb`: Jupyter Notebook detailing the machine learning pipeline.

## Datasets

The datasets used in this project are simulated IoT sensor readings, including temperature, humidity, and device operational statuses. These datasets are typically streamed through Kafka and stored in MongoDB for processing. The dataset is highly confidential and not publicly