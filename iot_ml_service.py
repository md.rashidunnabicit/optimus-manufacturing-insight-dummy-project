import pymongo
import pandas as pd
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.preprocessing import StandardScaler
from datetime import datetime
import logging
import pickle
from flask import Flask, request, jsonify
import threading
import time

# Set up logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

class MLModelService:
    def __init__(self, db_uri, db_name, collection_name):
        """
        Initializes the service by connecting to MongoDB and setting up data retrieval.
        """
        self.client = pymongo.MongoClient(db_uri)
        self.db = self.client[db_name]
        self.collection = self.db[collection_name]
        self.model = None
        self.scaler = None
        self.app = Flask(__name__)
        self.setup_routes()

    def load_data(self):
        """
        Load data from MongoDB for machine learning.
        """
        try:
            data = pd.DataFrame(list(self.collection.find()))
            logging.info("Data loaded successfully from MongoDB.")
            return data
        except Exception as e:
            logging.error(f"Error loading data from MongoDB: {e}")
            return pd.DataFrame()

    def preprocess_data(self, data):
        """
        Preprocess and prepare data for training.
        """
        data['timestamp'] = pd.to_datetime(data['timestamp'])
        data = data.set_index('timestamp')
        data = data.sort_index()
        features = ['temperature', 'humidity', 'pressure']
        target = 'equipment_failure'
        data = data.dropna(subset=features + [target])
        
        X = data[features]
        y = data[target]
        
        self.scaler = StandardScaler()
        X_scaled = self.scaler.fit_transform(X)
        
        logging.info("Data preprocessing complete.")
        return X_scaled, y

    def train_model(self, X, y):
        """
        Train a machine learning model using the provided data.
        """
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
        parameters = {'n_estimators': [50, 100, 200], 'max_depth': [5, 10, 20]}
        model = RandomForestRegressor(random_state=42)
        clf = GridSearchCV(model, parameters, cv=3)
        clf.fit(X_train, y_train)
        self.model = clf.best_estimator_
        
        # Evaluate the model
        predictions = self.model.predict(X_test)
        mse = mean_squared_error(y_test, predictions)
        r2 = r2_score(y_test, predictions)
        
        logging.info(f"Model trained with MSE: {mse} and R2: {r2}")
        return mse, r2

    def save_model(self):
        """
        Save the trained model for later use or deployment.
        """
        with open('model.pkl', 'wb') as f:
            pickle.dump(self.model, f)
        with open('scaler.pkl', 'wb') as f:
            pickle.dump(self.scaler, f)
        logging.info("Model and scaler saved to disk.")

    def load_model(self):
        """
        Load a trained model from disk.
        """
        with open('model.pkl', 'rb') as f:
            self.model = pickle.load(f)
        with open('scaler.pkl', 'rb') as f:
            self.scaler = pickle.load(f)
        logging.info("Model and scaler loaded from disk.")

    def setup_routes(self):
        """
        Set up Flask routes for API access to the model.
        """
        @self.app.route('/predict', methods=['POST'])
        def predict():
            try:
                json_input = request.json
                input_data = [[json_input['temperature'], json_input['humidity'], json_input['pressure']]]
                input_scaled = self.scaler.transform(input_data)
                prediction = self.model.predict(input_scaled)
                return jsonify({'prediction': prediction[0]})
            except Exception as e:
                logging.error(f"Error in prediction: {e}")
                return jsonify({'error': str(e)}), 400

    def start_api(self):
        """
        Start the Flask API to serve predictions.
        """
        self.app.run(port=5000)

    def start_background_tasks(self):
        """
        Start background tasks for monitoring and maintaining the service.
        """
        def monitor_health():
            while True:
                logging.info("System check at: " + str(datetime.now()))
                time.sleep(3600)  # Sleep for an hour

        thread = threading.Thread(target=monitor_health)
        thread.start()
        logging.info("Started background monitoring task.")

# Usage
if __name__ == "__main__":
    service = MLModelService('mongodb://localhost:27017/', 'iot_data', 'sensor_readings')
    data = service.load_data()
    if not data.empty:
        X, y = service.preprocess_data(data)
        service.train_model(X, y)
        service.save_model()
        service.start_background_tasks()
        service.start_api()
