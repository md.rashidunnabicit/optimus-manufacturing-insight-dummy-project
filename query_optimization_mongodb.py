import pymongo
from pymongo import MongoClient
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

class MongoDBAnalysis:
    def __init__(self, uri='mongodb://localhost:27017/', db_name='iot_data', collection_name='sensor_readings'):
        self.client = MongoClient(uri)
        self.db = self.client[db_name]
        self.collection = self.db[collection_name]

    def perform_complex_queries(self):
        """
        Execute and visualize a series of complex MongoDB queries.
        """
        self.aggregate_by_status()
        self.time_series_analysis()
        self.detect_outliers()

    def aggregate_by_status(self):
        """
        Aggregate average temperature and humidity by status.
        """
        pipeline = [
            {"$match": {"status": {"$exists": True}}},
            {"$group": {"_id": "$status", "average_temperature": {"$avg": "$temperature"}, "average_humidity": {"$avg": "$humidity"}}},
            {"$sort": {"average_temperature": -1}}
        ]
        results = list(self.collection.aggregate(pipeline))
        self.visualize_aggregation(results)

    def time_series_analysis(self):
        """
        Perform time series analysis for temperature over a specific period.
        """
        pipeline = [
            {"$match": {"timestamp": {"$gte": pd.Timestamp('2023-01-01'), "$lt": pd.Timestamp('2023-12-31')}}},
            {"$project": {"day": {"$dayOfYear": "$timestamp"}, "temperature": 1}},
            {"$group": {"_id": "$day", "average_temp": {"$avg": "$temperature"}}},
            {"$sort": {"_id": 1}}
        ]
        results = list(self.collection.aggregate(pipeline))
        self.plot_time_series(results)

    def detect_outliers(self):
        """
        Detect and visualize outliers in temperature readings.
        """
        pipeline = [
            {"$group": {
                "_id": None,
                "std_dev": {"$stdDevSamp": "$temperature"},
                "avg_temp": {"$avg": "$temperature"}
            }},
            {"$project": {
                "cutoff_high": {"$add": ["$avg_temp", {"$multiply": [3, "$std_dev"]}]},
                "cutoff_low": {"$subtract": ["$avg_temp", {"$multiply": [3, "$std_dev"]}]}
            }}
        ]
        limits = list(self.collection.aggregate(pipeline))[0]
        data = self.collection.find({"temperature": {"$not": {"$gte": limits['cutoff_low'], "$lte": limits['cutoff_high']}}})
        self.plot_outliers(list(data))

    def visualize_aggregation(self, data):
        """
        Visualize aggregation results.
        """
        df = pd.DataFrame(data)
        plt.figure(figsize=(10, 5))
        sns.barplot(x='_id', y='average_temperature', data=df)
        plt.title('Average Temperature by Equipment Status')
        plt.ylabel('Temperature (°C)')
        plt.xlabel('Status')
        plt.show()

    def plot_time_series(self, data):
        """
        Plot time series data.
        """
        df = pd.DataFrame(data).rename(columns={'_id': 'Day of the Year'})
        plt.figure(figsize=(15, 7))
        sns.lineplot(x='Day of the Year', y='average_temp', data=df)
        plt.title('Daily Average Temperature Throughout the Year')
        plt.ylabel('Average Temperature (°C)')
        plt.xlabel('Day of the Year')
        plt.show()

    def plot_outliers(self, data):
        """
        Plot outliers.
        """
        df = pd.DataFrame(data)
        plt.figure(figsize=(10, 6))
        sns.boxplot(x=df["temperature"])
        plt.title('Outlier Temperature Readings')
        plt.xlabel('Temperature (°C)')
        plt.show()
